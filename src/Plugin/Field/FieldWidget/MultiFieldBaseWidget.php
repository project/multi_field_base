<?php

namespace Drupal\multi_field_base\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A base class to faciliate declaration of field widgets.
 *
 * This base class is derived to faciliate declaration of field widgets
 * for field types declared wth multi_field_base module.
 */
abstract class MultiFieldBaseWidget extends WidgetBase {

  /**
   * Returns all the metadata necessary to infer property defintion.
   *
   * @return mixed
   *   metadata about properties
   */
  abstract public static function getAllProperties();

  /**
   * Returns the default settings according to metadata about properties.
   *
   * @return mixed
   *   default settings
   */
  protected static function defaultSettingsMultiFieldBase() {
    $ret = [];
    $props = static::getAllProperties();
    foreach ($props as $prop_key => $prop_data) {
      $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
      switch ($prop_data_type) {
        case "float":
        case "integer":
          $ret[$prop_key] = [
            'placeholder' => '',
          ];
          break;

        case 'string':
          $ret[$prop_key] = [
            'size' => 60,
            'placeholder' => '',
          ];
          break;
      }
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array_merge_recursive(static::defaultSettingsMultiFieldBase(), parent::defaultSettings());
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $props = static::getAllProperties();
    foreach ($props as $prop_key => $prop_data) {
      $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
      $elements[$prop_key] = [
        "#type" => "details",
        "#open" => TRUE,
        "#tree" => TRUE,
        "#title" => (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key),
      ];
      switch ($prop_data_type) {
        case "float":
        case "integer":
          $elements[$prop_key]['placeholder'] = [
            '#type' => 'textfield',
            '#title' => t('Placeholder'),
            '#default_value' => $this->getSetting($prop_key)['placeholder'],
            '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
          ];
          break;

        case "string":
          $string_settings = $this->getSetting($prop_key);
          $size = array_key_exists("size", $string_settings) ? $string_settings["size"] : 60;
          $elements[$prop_key]['size'] = [
            '#type' => 'number',
            '#title' => t('Size of textfield'),
            '#default_value' => $size,
            '#required' => TRUE,
            '#min' => 1,
          ];
          $elements[$prop_key]['placeholder'] = [
            '#type' => 'textfield',
            '#title' => t('Placeholder'),
            '#default_value' => $this->getSetting($prop_key)['placeholder'],
            '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
          ];
          break;
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $props = static::getAllProperties();

    foreach ($props as $prop_key => $prop_data) {
      $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
      $title = (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key);
      switch ($prop_data_type) {
        case "float":
        case "integer":
          $placeholder = $this->getSetting($prop_key)['placeholder'];
          if (!empty($placeholder)) {
            $summary[] = $title . " -> " . t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
          }
          break;

        case "string":
          $summary[] = $title . " -> " . t('Textfield size: @size', ['@size' => $this->getSetting($prop_key)['size']]);
          $placeholder = $this->getSetting($prop_key)['placeholder'];
          if (!empty($placeholder)) {
            $summary[] = $title . " -> " . t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
          }
          break;
      }
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $props = static::getAllProperties();

    $final_element = [];

    foreach ($props as $prop_key => $prop_data) {
      $prop_data_type = array_key_exists("type", $prop_data) ? $prop_data["type"] : "string";
      $title = (array_key_exists("title", $prop_data) ? $prop_data["title"] : $prop_key);

      switch ($prop_data_type) {
        case "float":
        case "integer":
          $value = isset($items[$delta]->value) ? $items[$delta]->value : NULL;

          $myelement = $element + [
            '#type' => 'number',
            '#default_value' => $value,
            '#placeholder' => $this->getSetting($prop_key)['placeholder'],
          ];

          // Set the step for floating point and decimal numbers.
          // $this->fieldDefinition->getType()) {.
          switch ($prop_data_type) {
            /* case 'decimal':
            $myelement['#step'] = pow(0.1, $field_settings['scale']);
            break;
             */

            case 'float':
              $myelement['#step'] = 'any';
              break;
          }

          // Set minimum and maximum.
          if (array_key_exists("min", $prop_data) && is_numeric($prop_data['min'])) {
            $myelement['#min'] = $prop_data['min'];
          }
          if (array_key_exists("max", $prop_data) &&is_numeric($prop_data['max'])) {
            $myelement['#max'] = $prop_data['max'];
          }

          // Add prefix and suffix.
          if (array_key_exists("max", $prop_data) && $prop_data['prefix']) {
            $prefixes = explode('|', $prop_data['prefix']);
            $myelement['#field_prefix'] = FieldFilteredMarkup::create(array_pop($prefixes));
          }
          if (array_key_exists("max", $prop_data) && $prop_data['suffix']) {
            $suffixes = explode('|', $prop_data['suffix']);
            $myelement['#field_suffix'] = FieldFilteredMarkup::create(array_pop($suffixes));
          }

          $final_element[$prop_key] = $myelement;
          // $final_element[$prop_key]["#title"]. " ".$title
          $final_element[$prop_key]["#title"] = $title;
          break;

        case "string":
          $max_length = array_key_exists("max_length", $prop_data) ? $prop_data["max_length"] : 255;
          $final_element[$prop_key] = $element + [
            '#type' => 'textfield',
            '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
            '#size' => $this->getSetting($prop_key)['size'],
            '#placeholder' => $this->getSetting($prop_key)['placeholder'],
            '#maxlength' => $max_length,
            '#attributes' => ['class' => ['js-text-full', 'text-full']],
          ];
          $final_element[$prop_key]["#title"] = $title;

          break;
      }
    }
    return $final_element;
  }

}
