<?php

namespace Drupal\multi_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\multi_field_base\Plugin\Field\FieldType\MultiFieldBase;

/**
 *
 * @FieldType(
 *   id = "multi_field_base_test_fields_single_integer_unsigned",
 *   label = @Translation("multi_field_base_test_fields: single_string_unsigned"),
 *   description = @Translation("") * )
 */
class TestIntegerUnsigned extends MultiFieldBase {

  /**
   *
   */
  public static function getAllProperties() {
    $props = [];
    $props['mykey'] = [
      "title" => "title",
      "unsigned" => TRUE,
      "type" => "integer",
      "required" => FALSE,
    ];
    return $props;

  }

}
