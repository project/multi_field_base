<?php

namespace Drupal\multi_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\multi_field_base\Plugin\Field\FieldType\MultiFieldBase;

/**
 *
 * @FieldType(
 *   id = "multi_field_base_test_fields_single_float",
 *   label = @Translation("multi_field_base_test_fields: single_float"),
 *   description = @Translation("") * )
 */
class TestFloat extends MultiFieldBase {

  /**
   *
   */
  public static function getAllProperties() {
    $props = [];
    $props['mykey'] = [
      "title" => "title",
      "type" => "float",
      "required" => FALSE,
    ];
    return $props;

  }

}
