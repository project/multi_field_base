<?php

namespace Drupal\multi_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\multi_field_base\Plugin\Field\FieldType\MultiFieldBase;

/**
 *
 * @FieldType(
 *   id = "multi_field_base_test_fields_single_map",
 *   label = @Translation("multi_field_base_test_fields: single_map"),
 *   description = @Translation("") * )
 */
class TestMap extends MultiFieldBase {

  /**
   *
   */
  public static function getAllProperties() {
    $props = [];
    $props['one'] = [
      "type" => "integer",
    ];
    $props['two'] = [
      "type" => "float",
    ];
    $props['three'] = [
      "type" => "string",
    ];
    $props['value'] = [
      "type" => "string",
    ];

    return $props;

  }

}
