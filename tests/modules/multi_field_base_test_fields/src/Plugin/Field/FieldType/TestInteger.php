<?php

namespace Drupal\multi_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\multi_field_base\Plugin\Field\FieldType\MultiFieldBase;

/**
 *
 * @FieldType(
 *   id = "multi_field_base_test_fields_single_integer",
 *   label = @Translation("multi_field_base_test_fields: single_string"),
 *   description = @Translation("") * )
 */
class TestInteger extends MultiFieldBase {

  /**
   *
   */
  public static function getAllProperties() {
    $props = [];
    $props['mykey'] = [
      "title" => "title",
      "type" => "integer",
      "required" => FALSE,
    ];
    return $props;

  }

}
