<?php

namespace Drupal\multi_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\multi_field_base\Plugin\Field\FieldType\MultiFieldBase;

/**
 *
 * @FieldType(
 *   id = "multi_field_base_test_fields_single_string_max_length",
 *   label = @Translation("multi_field_base_test_fields: single_string_max_length"),
 *   description = @Translation("") * )
 */
class TestStringMaxLength extends MultiFieldBase {

  /**
   *
   */
  public static function getAllProperties() {
    $props = [];
    $props['mykey'] = [
      "title" => "String title",
      "type" => "string",
      "required" => FALSE,
      "max_length" => 20,

    ];
    return $props;

  }

}
