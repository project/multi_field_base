<?php

namespace Drupal\multi_field_base_test_fields\Plugin\Field\FieldType;

use Drupal\multi_field_base\Plugin\Field\FieldType\MultiFieldBase;

/**
 *
 * @FieldType(
 *   id = "multi_field_base_test_fields_single_string",
 *   label = @Translation("multi_field_base_test_fields: single_string"),
 *   description = @Translation("") * )
 */
class TestString extends MultiFieldBase {

  /**
   *
   */
  public static function getAllProperties() {
    $props = [];
    $props['mykey'] = [
      "title" => "String title",
      "type" => "string",
      "required" => FALSE,

    ];
    return $props;

  }

}
