<?php

namespace Drupal\Tests\multi_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group multi_field_base
 */
class MultiFieldBaseTestString extends MultiFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "multi_field_base_test_fields_single_string";
    parent::setUp();
  }

  /**
   *
   */
  public function testReadWrite() {
    $value = $this->randomString();
    $value .= "\n\n<strong>" . $this->randomString() . '</strong>';
    $value .= "\n\n" . $this->randomString();

    $entity = EntityTest::create([]);
    $get_value = $entity->{$this->fieldName}[] = [
      "mykey" => $value,
    ];
    // $entity->{$this->fieldName}->mykey = $value;
    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];

    $this->assertEqual($get_value, $value);

    // -----
    $entity = EntityTest::create([]);
    $entity->{$this->fieldName}->mykey = $value;

    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];
  }

}
