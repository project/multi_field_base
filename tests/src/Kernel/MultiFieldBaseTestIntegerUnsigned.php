<?php

namespace Drupal\Tests\multi_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group multi_field_base
 */
class MultiFieldBaseTestIntegerUnsigned extends MultiFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "multi_field_base_test_fields_single_integer_unsigned";
    parent::setUp();

  }

  /**
   *
   */
  public function valuesProvider() {
    return [
          ["value" => -1],
          ["value" => -22],
    ];

  }

  /**
   * Tests string formatter output.
   *
   * @dataProvider valuesProvider
   */
  public function testReadWrite($value) {

    $entity = EntityTest::create([]);
    try {
      $get_value = $entity->{$this->fieldName}[] = [
        "mykey" => $value,
      ];
      $this->fail("expected exception");
    }
    catch (\Exception $e) {
      $this->pass("expected exception ok");
    }
  }

}
