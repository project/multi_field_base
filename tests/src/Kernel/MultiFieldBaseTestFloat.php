<?php

namespace Drupal\Tests\multi_field_base\Kernel;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests the raw string formatter.
 *
 * @group multi_field_base
 */
class MultiFieldBaseTestFloat extends MultiFieldBaseTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fieldType = "multi_field_base_test_fields_single_float";
    parent::setUp();

  }

  /**
   *
   */
  public function valuesProvider() {
    return [
        ["value" => 0.334],
          ["value" => 0.0],
          ["value" => 1234.66666],
          ["value" => 4565.66666],
    ];

  }

  /**
   * Tests string formatter output.
   *
   * @dataProvider valuesProvider
   */
  public function testReadWrite($value) {
    $entity = EntityTest::create([]);
    $get_value = $entity->{$this->fieldName}[] = [
      "mykey" => $value,
    ];
    // $entity->{$this->fieldName}->mykey = $value;
    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];

    $this->assertEqual($get_value, $value);
    // -----
    $entity = EntityTest::create([]);
    $entity->{$this->fieldName}->mykey = $value;

    $get_value = $entity->{$this->fieldName}->get(0)->getValue()["mykey"];
    $this->assertEqual($get_value, $value);
  }

}
