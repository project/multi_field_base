<?php

namespace Drupal\multi_field_example\Plugin\Field\FieldType;

use Drupal\multi_field_base\Plugin\Field\FieldType\MultiFieldBase;

/**
 *
 * @FieldType(
 *   id = "multi_field_example",
 *   label = @Translation("example of multi_field_base field type"),
 *   default_widget = "multi_field_example",
 *   default_formatter= "multi_field_example"
 * )
 */
class MultiFieldExample extends MultiFieldBase {

  /**
   *
   */
  public static function getAllProperties() {
    $props = [];
    $props['one'] = [
      "type" => "integer",
    ];
    $props['two'] = [
      "type" => "float",
    ];
    $props['three'] = [
      "type" => "string",
    ];
    return $props;
  }

}
