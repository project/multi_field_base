<?php

namespace Drupal\multi_field_example\Plugin\Field\FieldFormatter;

use Drupal\multi_field_example\Plugin\Field\FieldType\MultiFieldExample;
use Drupal\multi_field_base\Plugin\Field\FieldFormatter\MultiFieldBaseFormatter;

/**
 *
 * @FieldFormatter(
 *   id = "multi_field_example",
 *   label = @Translation("example of multi_field_base field formatter"),
 *   field_types = {
 *     "multi_field_example"
 *   }
 * )
 */
class MultiFieldExampleFormatter extends MultiFieldBaseFormatter {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    return MultiFieldExample::getAllProperties();
  }

}
