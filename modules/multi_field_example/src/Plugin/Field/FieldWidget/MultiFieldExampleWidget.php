<?php

namespace Drupal\multi_field_example\Plugin\Field\FieldWidget;

use Drupal\multi_field_example\Plugin\Field\FieldType\MultiFieldExample;
use Drupal\multi_field_base\Plugin\Field\FieldWidget\MultiFieldBaseWidget;

/**
 *
 * @FieldWidget(
 *   id = "multi_field_example",
 *   module = "multi_field_example",
 *   label = @Translation("example of multi_field_base field widget"),
 *   field_types = {
 *     "multi_field_example"
 *   }
 * )
 */
class MultiFieldExampleWidget extends MultiFieldBaseWidget {

  /**
   * {@inheritdoc}
   */
  public static function getAllProperties() {
    return MultiFieldExample::getAllProperties();
  }

}
